﻿using Xamarin.Forms;

namespace Calicosol
{
    public class DemoApp : Application
    {
        public DemoApp()
        {
            var page = new MainPage();
            page.BindingContext = new MainViewModel(this);
            MainPage = new NavigationPage(page);
        }

        public async void NavigateTo(Page page)
        {
            await ((NavigationPage)MainPage).PushAsync(page);
        }
    }
}
