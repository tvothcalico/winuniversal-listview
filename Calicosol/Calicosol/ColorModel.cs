﻿using Xamarin.Forms;

namespace Calicosol
{
    class ColorModel
    {
        public Color Color { get; set; }

        public string Text { get; set; }

        public Color TextColor { get; set; }

        public ColorModel(Color color, string text, Color textColor)
        {
            Color = color;
            Text = text;
            TextColor = textColor;
        }
    }
}
