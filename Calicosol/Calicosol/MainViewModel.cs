﻿using Xamarin.Forms;
using System.Collections.Generic;

namespace Calicosol
{
    public class MainViewModel : ViewModelBase
    {
        private string _labelText;
        public List<ListModel> _listItems;

        public MainViewModel(DemoApp app) : base(app)
        {
            LabelText = "Default Label Text";
            List<ListModel> items = new List<ListModel>();
            for (int i = 0; i < 16; ++i)
            {
                ListModel lm = new ListModel() { Id = i, Text = string.Format("{0:00}", i) };
                items.Add(lm);
            }
            ListItems = items;
        }

        public string LabelText
        {
            get { return _labelText; }
            set
            {
                if (_labelText != value)
                {
                    _labelText = value;
                    OnPropertyChanged();
                }
            }
        }
        
        public List<ListModel> ListItems
        {
            get { return _listItems; }
            set
            {
                if (_listItems != value)
                {
                    _listItems = value;
                    OnPropertyChanged();
                }
            }
        }

        public class ListModel
        {
            public ImageSource DisplayIcon
            {
                get { return ImageSource.FromResource("Calicosol.Icon.png"); }
            }
            public int Id { get; set; }
            public string Text { get; set; }
        }
    }
}
